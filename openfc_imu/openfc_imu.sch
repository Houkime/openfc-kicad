EESchema Schematic File Version 5
LIBS:openfc_imu-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2200 5400 2500 5400
Wire Wire Line
	2600 3300 1900 3300
Wire Wire Line
	3800 3400 4100 3400
Wire Wire Line
	4100 3400 4100 3500
Wire Wire Line
	2600 2800 2400 2800
Wire Wire Line
	2400 2800 2400 2400
Wire Wire Line
	2400 2400 4000 2400
Wire Wire Line
	4000 2400 4000 3000
Wire Wire Line
	4000 3000 4800 3000
Text Label 4800 3000 2    70   ~ 0
IMU_MISO
Wire Wire Line
	3900 5100 4800 5100
Text Label 4800 5100 2    70   ~ 0
IMU_MISO
Wire Wire Line
	8600 5300 9500 5300
Text Label 9500 5300 2    70   ~ 0
IMU_MISO
Wire Wire Line
	5900 3000 7200 3000
Text Label 5900 3000 0    10   ~ 0
IMU_MISO
Wire Wire Line
	3800 3100 4800 3100
Text Label 4800 3100 2    70   ~ 0
IMU_SCLK
Wire Wire Line
	3900 5000 4800 5000
Text Label 4800 5000 2    70   ~ 0
IMU_SCLK
Wire Wire Line
	8600 5500 9500 5500
Text Label 9500 5500 2    70   ~ 0
IMU_SCLK
Wire Wire Line
	5900 3100 7200 3100
Text Label 5900 3100 0    10   ~ 0
IMU_SCLK
Wire Wire Line
	3800 3200 4800 3200
Text Label 4800 3200 2    70   ~ 0
IMU_MOSI
Wire Wire Line
	3900 5200 4800 5200
Text Label 4800 5200 2    70   ~ 0
IMU_MOSI
Wire Wire Line
	8600 5100 9500 5100
Text Label 9500 5100 2    70   ~ 0
IMU_MOSI
Wire Wire Line
	5900 2900 7200 2900
Text Label 5900 2900 0    10   ~ 0
IMU_MOSI
Wire Wire Line
	3800 3700 4200 3700
Wire Wire Line
	4200 3700 4200 3300
Wire Wire Line
	4200 3300 4800 3300
Text Label 4800 3300 2    70   ~ 0
IMU_MAG_INT
Wire Wire Line
	8600 3300 9900 3300
Text Label 8600 3300 0    10   ~ 0
IMU_MAG_INT
Wire Wire Line
	2500 5000 2400 5000
Wire Wire Line
	2400 5000 2400 4700
Wire Wire Line
	2400 4700 3900 4700
Wire Wire Line
	3900 4700 3900 4800
Wire Wire Line
	3900 4800 4800 4800
Text Label 4800 4800 2    70   ~ 0
IMU_MPU_INT
Wire Wire Line
	8600 2900 9900 2900
Text Label 8600 2900 0    10   ~ 0
IMU_MPU_INT
Wire Wire Line
	3900 4900 4800 4900
Text Label 4800 4900 2    70   ~ 0
IMU_MPU_CS
Wire Wire Line
	8600 2800 9900 2800
Text Label 8600 2800 0    10   ~ 0
IMU_MPU_CS
Wire Wire Line
	7200 4800 7200 4400
Wire Wire Line
	7200 4400 8700 4400
Wire Wire Line
	8700 4400 8700 4900
Wire Wire Line
	8700 4900 9500 4900
Text Label 9500 4900 2    70   ~ 0
IMU_BARO_CS
Wire Wire Line
	8600 3000 9900 3000
Text Label 8600 3000 0    10   ~ 0
IMU_BARO_CS
Wire Wire Line
	3800 2900 4800 2900
Text Label 4800 2900 2    70   ~ 0
IMU_MAG_CS
Wire Wire Line
	8600 3200 9900 3200
Text Label 8600 3200 0    10   ~ 0
IMU_MAG_CS
Wire Wire Line
	8600 3100 9900 3100
Wire Wire Line
	3800 2700 4200 2700
Wire Wire Line
	4200 2700 4200 2500
Wire Wire Line
	4200 2500 4300 2500
Wire Wire Line
	4300 2500 4300 2300
Wire Wire Line
	3800 2800 4300 2800
Wire Wire Line
	4300 2800 4300 2500
Connection ~ 4300 2500
Text Label 3800 2700 0    10   ~ 0
IMU+3V3
Wire Wire Line
	2200 4900 2500 4900
Wire Wire Line
	2200 4600 2200 4900
Connection ~ 2200 4900
Text Label 2200 4900 0    10   ~ 0
IMU+3V3
Wire Wire Line
	7200 5100 6600 5100
Wire Wire Line
	6600 5200 6600 5100
Wire Wire Line
	6600 5100 6600 4500
Connection ~ 6600 5100
Text Label 7200 5100 0    10   ~ 0
IMU+3V3
Wire Wire Line
	5900 2800 7200 2800
Text Label 5900 2800 0    10   ~ 0
IMU+3V3
Wire Wire Line
	5900 3200 7200 3200
Text Label 5900 3200 0    10   ~ 0
IMU_GND
Wire Wire Line
	1900 3600 2400 3600
Wire Wire Line
	2400 3600 2600 3600
Wire Wire Line
	2600 3600 2600 3500
Wire Wire Line
	1900 3600 1900 3900
Wire Wire Line
	2600 2900 2400 2900
Wire Wire Line
	2400 2900 2400 3600
Connection ~ 1900 3600
Connection ~ 2600 3600
Connection ~ 2400 3600
Text Label 1900 3600 0    10   ~ 0
IMU_GND
Wire Wire Line
	1900 4900 1900 5100
Wire Wire Line
	1900 5100 1900 5200
Wire Wire Line
	1900 5200 1900 5300
Wire Wire Line
	1900 5300 1900 5400
Wire Wire Line
	1900 5400 1900 5500
Wire Wire Line
	1900 5500 1900 5700
Wire Wire Line
	2500 5100 1900 5100
Wire Wire Line
	2200 5300 1900 5300
Wire Wire Line
	2500 5500 1900 5500
Wire Wire Line
	2500 5200 1900 5200
Connection ~ 1900 5100
Connection ~ 1900 5200
Connection ~ 1900 5300
Connection ~ 1900 5400
Connection ~ 1900 5500
Text Label 1900 4900 0    10   ~ 0
IMU_GND
Wire Wire Line
	6600 5700 6600 5500
Wire Wire Line
	7200 5500 6600 5500
Wire Wire Line
	7200 5300 7200 5500
Connection ~ 6600 5500
Connection ~ 7200 5500
Text Label 6600 5700 0    10   ~ 0
IMU_GND
Wire Wire Line
	4600 2500 4700 2500
Text Label 4600 2500 0    10   ~ 0
IMU_GND
Wire Wire Line
	5900 3300 7200 3300
Text Label 5900 3300 0    10   ~ 0
IMU_GND
$Comp
L openfc_imu-eagle-import:MPU-6000QFN-24 U2
U 1 1 C2DEB937
P 3200 5200
F 0 "U2" H 2700 5600 59  0000 L BNN
F 1 "MPU-6000QFN-24" H 2700 4700 59  0000 L BNN
F 2 "openfc_imu:QFN-24-TESTING" H 3200 5200 50  0001 C CNN
F 3 "http://datasheet.octopart.com/MPU-6000-InvenSense-datasheet-14421344.pdf" H 3200 5200 50  0001 C CNN
	1    3200 5200
	1    0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:C-EUC0603 C6
U 1 1 FE64C8E2
P 2300 5300
F 0 "C6" H 2360 5315 59  0000 L BNN
F 1 "100n" H 2360 5115 59  0000 L BNN
F 2 "openfc_imu:C0603" H 2300 5300 50  0001 C CNN
F 3 "" H 2300 5300 50  0001 C CNN
	1    2300 5300
	0    -1   -1   0   
$EndComp
$Comp
L openfc_imu-eagle-import:C-EUC0603 C11
U 1 1 59663956
P 2000 4900
F 0 "C11" H 2060 4915 59  0000 L BNN
F 1 "100n" H 2060 4715 59  0000 L BNN
F 2 "openfc_imu:C0603" H 2000 4900 50  0001 C CNN
F 3 "" H 2000 4900 50  0001 C CNN
	1    2000 4900
	0    -1   -1   0   
$EndComp
$Comp
L openfc_imu-eagle-import:C-EUC0603 C12
U 1 1 16466EF9
P 2000 5400
F 0 "C12" H 2060 5415 59  0000 L BNN
F 1 "2.2nf" H 2060 5215 59  0000 L BNN
F 2 "openfc_imu:C0603" H 2000 5400 50  0001 C CNN
F 3 "" H 2000 5400 50  0001 C CNN
	1    2000 5400
	0    -1   -1   0   
$EndComp
$Comp
L openfc_imu-eagle-import:GND #GND01
U 1 1 DB8695AC
P 1900 5800
F 0 "#GND01" H 1900 5800 50  0001 C CNN
F 1 "GND" H 1800 5700 59  0000 L BNN
F 2 "" H 1900 5800 50  0001 C CNN
F 3 "" H 1900 5800 50  0001 C CNN
	1    1900 5800
	1    0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:+3V3 #+3V03
U 1 1 A56F219E
P 2200 4500
F 0 "#+3V03" H 2200 4500 50  0001 C CNN
F 1 "+3V3" V 2100 4300 59  0000 L BNN
F 2 "" H 2200 4500 50  0001 C CNN
F 3 "" H 2200 4500 50  0001 C CNN
	1    2200 4500
	-1   0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:MS5611-01BA03 IC1
U 1 1 06F92037
P 7900 5100
F 0 "IC1" H 7400 5650 59  0000 L BNN
F 1 "MS5611-01BA03" H 7400 4450 59  0000 L BNN
F 2 "openfc_imu:SON125P300X500X100-8N" H 7900 5100 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=MS5611-01BA03&DocType=Data+Sheet&DocLang=English" H 7900 5100 50  0001 C CNN
	1    7900 5100
	1    0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:C-EUC0603 C13
U 1 1 9AFB8C1C
P 6600 5300
F 0 "C13" H 6660 5315 59  0000 L BNN
F 1 "100n" H 6660 5115 59  0000 L BNN
F 2 "openfc_imu:C0603" H 6600 5300 50  0001 C CNN
F 3 "" H 6600 5300 50  0001 C CNN
	1    6600 5300
	1    0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:GND #GND06
U 1 1 A07C0193
P 6600 5800
F 0 "#GND06" H 6600 5800 50  0001 C CNN
F 1 "GND" H 6500 5700 59  0000 L BNN
F 2 "" H 6600 5800 50  0001 C CNN
F 3 "" H 6600 5800 50  0001 C CNN
	1    6600 5800
	1    0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:+3V3 #+3V04
U 1 1 E81106EE
P 6600 4400
F 0 "#+3V04" H 6600 4400 50  0001 C CNN
F 1 "+3V3" V 6500 4200 59  0000 L BNN
F 2 "" H 6600 4400 50  0001 C CNN
F 3 "" H 6600 4400 50  0001 C CNN
	1    6600 4400
	1    0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:HMC5883LSMD U$1
U 1 1 6AAA3927
P 3200 3200
F 0 "U$1" H 2800 3800 59  0000 L BNN
F 1 "HMC5983" H 2800 2500 59  0000 L BNN
F 2 "openfc_imu:16LPCC" H 3200 3200 50  0001 C CNN
F 3 "https://www.farnell.com/datasheets/1802211.pdf" H 3200 3200 50  0001 C CNN
	1    3200 3200
	1    0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:C-EUC0603 C14
U 1 1 5C9E65F2
P 1900 3400
F 0 "C14" H 1960 3415 59  0000 L BNN
F 1 "4.7u" H 1960 3215 59  0000 L BNN
F 2 "openfc_imu:C0603" H 1900 3400 50  0001 C CNN
F 3 "" H 1900 3400 50  0001 C CNN
	1    1900 3400
	1    0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:+3V3 #+3V05
U 1 1 D30003A2
P 4300 2200
F 0 "#+3V05" H 4300 2200 50  0001 C CNN
F 1 "+3V3" V 4200 2000 59  0000 L BNN
F 2 "" H 4300 2200 50  0001 C CNN
F 3 "" H 4300 2200 50  0001 C CNN
	1    4300 2200
	-1   0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:GND #GND05
U 1 1 EF27308A
P 1900 4000
F 0 "#GND05" H 1900 4000 50  0001 C CNN
F 1 "GND" H 1800 3900 59  0000 L BNN
F 2 "" H 1900 4000 50  0001 C CNN
F 3 "" H 1900 4000 50  0001 C CNN
	1    1900 4000
	1    0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:C-EUC0603 C15
U 1 1 8EE80D5C
P 4000 3500
F 0 "C15" H 4060 3515 59  0000 L BNN
F 1 "220n" H 4060 3315 59  0000 L BNN
F 2 "openfc_imu:C0603" H 4000 3500 50  0001 C CNN
F 3 "" H 4000 3500 50  0001 C CNN
	1    4000 3500
	0    1    1    0   
$EndComp
$Comp
L openfc_imu-eagle-import:C-EUC0603 C16
U 1 1 E207C13D
P 4500 2500
F 0 "C16" H 4560 2515 59  0000 L BNN
F 1 "100n" H 4560 2315 59  0000 L BNN
F 2 "openfc_imu:C0603" H 4500 2500 50  0001 C CNN
F 3 "" H 4500 2500 50  0001 C CNN
	1    4500 2500
	0    1    1    0   
$EndComp
$Comp
L openfc_imu-eagle-import:GND #GND07
U 1 1 9CDE76EF
P 4700 2600
F 0 "#GND07" H 4700 2600 50  0001 C CNN
F 1 "GND" H 4600 2500 59  0000 L BNN
F 2 "" H 4700 2600 50  0001 C CNN
F 3 "" H 4700 2600 50  0001 C CNN
	1    4700 2600
	1    0    0    -1  
$EndComp
$Comp
L openfc_imu-eagle-import:SPIIMU U$5
U 1 1 6A689A17
P 7900 3100
F 0 "U$5" H 7400 3500 59  0000 L BNN
F 1 "SPIIMU" H 7400 2700 59  0000 L BNN
F 2 "openfc_imu:SPIIMU" H 7900 3100 50  0001 C CNN
F 3 "" H 7900 3100 50  0001 C CNN
	1    7900 3100
	1    0    0    -1  
$EndComp
NoConn ~ 2600 2700
NoConn ~ 2600 3000
NoConn ~ 2600 3100
NoConn ~ 3900 5300
NoConn ~ 3900 5400
NoConn ~ 3900 5500
Text Notes 7600 3650 0    50   ~ 0
Connector
Text Notes 7600 6000 0    50   ~ 0
Barometer
Text Notes 2850 6000 0    50   ~ 0
Gyroscope
Text Notes 2850 4100 0    50   ~ 0
Magnetometer
Text Notes 3450 7150 0    197  ~ 0
SENSOR BOARD
$EndSCHEMATC
