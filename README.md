# OpenFC

#### Open-Hardware Flight-Controller

* Standard-Size: 50x50mm (45x45mm holes) 
* CPU: STM32F405 @ 168MHZ
* Plop-board (IMU) with sensors:
  * Gyro/ACC: MPU6000 (SPI) 
  * Compass: HMC5983 (SPI)
  * Baro: MS5611 (SPI)

* Serial-Ports for GPS, Telemetry and SBUS
* 8 PWM-ESC-Connectors + CAN-Bus
* PPM and SBUS inputs
* microSD slot
* Autoquad software compatible (Board M4r6) (https://code.google.com/p/autoquad/)
* Also probably https://github.com/multigcs/quadfork
* (Calculated value from design) Seems like it needs arond 10.4 V LiPo input.


### Changes v0.9:

	* initial Version
	* 2 errors in the Layout, but working


### Changes v0.91: 

	* untested
	* bugfixes of v0.9
	* LED-Postion moved
	* SBUS-Inverter added
  
### Changes v0.92_kicad

  * Untested
  * Converted to kicad and fixed conversion bugs.
  * CAN transiever input voltage fixed to 5v (was wrongly fed with 3.3 v)
  * CAN transiever updated to a newer model
  * USB and CAN dpairs tuned-ish
  * Added 4.7 uf capacitor on VDD pin 48 of MCU as requested by datasheet.
  * ADJ pin on 5v -> 3.3v converter (IC1) fixed to ground (was floating).
  * Added missing capacitance values
  * Added some datasheets' links
  * Calculated that LiPo input voltage should be loosely around 10.4 volts
  * Hopefully improved magnetometer working conditions in compliance to ds.
  
  
### TODO:

  * Pressure sensor is too flimsy and clean-loving. Replace with sth more robust.
  * Gyroscope seems too expensive (and old).
  * Not all parts are rated for subzero temperatures
  * Current return paths need some love
  * No courtyards (so no real way to do proximity autochecks)
  * Rounded pads might be good
  * Compatibility with more accessible foss software (which is not on google code)
  * BOM seems a bit expensive - the board might be overengineered for what it needs to do.
  * Don't really know yet what extra SPI flash is for, might be software related. (MCU has internal 1 MB flash)

![v0.91 Board-Layout](https://raw.githubusercontent.com/multigcs/openfc/master/v0.91/openfc.png "v0.91 Board-Layout")

![v0.90 Board-Layout](https://raw.githubusercontent.com/multigcs/openfc/master/v0.9/openfc.png "v0.9 Board")


First IMU-Test: https://www.youtube.com/watch?v=cWaLLEZJb3Y

First Flight-Test: https://www.youtube.com/watch?v=trHpGgnNXYM

First Poshold-Test: https://www.youtube.com/watch?v=XDp27G_Ods4


